const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

module.exports = mongoose.model('ShoppingList',
  new mongoose.Schema({
    name: { type: String, required: true },
    owner: { kind: String, item: { type: ObjectId, refPath: 'owner.kind' }, required: true },
    items: [new mongoose.Schema({
        label: { type: String, required: true },
        done: { type: Boolean, required: true }
    })]
  })
)
