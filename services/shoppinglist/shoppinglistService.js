const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const restify = require('express-restify-mongoose')
const app = express()
const router = express.Router()
const ShoppingList = require('./model/shoppinglist')

const ObjectId = mongoose.Schema.Types.ObjectId

app.use(bodyParser.json())
app.use(methodOverride())

mongoose.connect('mongodb://localhost:27017/shoppinglist')

// Model
restify.serve(router, ShoppingList)

app.use(router)

app.listen(3002, () => {
  console.log('ShoppingList service listening on port 3002')
})