const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const restify = require('express-restify-mongoose')
const app = express()
const router = express.Router()

const ObjectId = mongoose.Schema.Types.ObjectId

app.use(bodyParser.json())
app.use(methodOverride())

mongoose.connect('mongodb://localhost:27017/shoppinglist')

// Model
restify.serve(router, mongoose.model('User', new mongoose.Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  name: { type: String },
})))

app.use(router)

app.listen(3003, () => {
  console.log('User service listening on port 3003')
})