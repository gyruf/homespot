const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const restify = require('express-restify-mongoose')
const app = express()
const router = express.Router()

app.use(bodyParser.json())
app.use(methodOverride())

mongoose.connect('mongodb://localhost:27017/household')

// Model
restify.serve(router, mongoose.model('Household', new mongoose.Schema({
  name: { type: String, required: true },
  address: { type: String }
})))

app.use(router)

app.listen(3001, () => {
  console.log('Household service listening on port 3001')
})