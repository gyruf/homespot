const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const mongoose = require('mongoose')
const mockgoose = require('mockgoose')
const ShoppingList = require('./../../services/shoppinglist/model/shoppinglist')
chai.use(chaiAsPromised)
mongoose.Promise = global.Promise

describe("ShoppingList", function(){
    before(function(done) {
        mockgoose(mongoose).then(function() {
            mongoose.connect('mongodb://localhost:27017/test', done);
        })
    })

    after(function(done) {
        mongoose.reset(done)
    })

    it("test", function() {
        const s = new ShoppingList({name: 'test'})
        s.save().then(function() {
            ShoppingList.find().then(function(shoppinglists) {
                expect(shoppinglists).not.to.be.equal(undefined)
            })
        })
    })
})